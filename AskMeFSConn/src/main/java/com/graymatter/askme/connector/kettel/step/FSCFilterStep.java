package com.graymatter.askme.connector.kettel.step;

import org.pentaho.di.core.Condition;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.filterrows.FilterRowsMeta;

import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;
import com.graymatter.askme.connector.exception.FSCErrorCode;
import com.graymatter.askme.connector.exception.FSCException;
import com.graymatter.askme.connector.kettel.general.FSCGeneralStep;

public class FSCFilterStep extends FSCGeneralStep {
	
	private Condition getCondition(){
		
		Condition condition = new Condition();
		condition.setLeftValuename(ASKOutputConstants.TAG_REFERENCE);
		condition.setFunction(Condition.FUNC_NOT_NULL);
		return condition;
	}

	@Override
	public StepMetaInterface getStep() throws Exception {
		FilterRowsMeta filterRowsMeta = null;
		try {
			filterRowsMeta = new FilterRowsMeta();
			filterRowsMeta.setCondition(getCondition());
		} catch (Exception e) {
			e.printStackTrace();
			throw new FSCException(FSCErrorCode.Mapping.MAPPING_GENERIC);
		} 
		return filterRowsMeta;
	}

}
