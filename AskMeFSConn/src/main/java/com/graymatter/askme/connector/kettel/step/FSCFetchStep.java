package com.graymatter.askme.connector.kettel.step;

import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.getfilenames.GetFileNamesMeta;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.FSCEnvironment;
import com.graymatter.askme.connector.exception.FSCErrorCode;
import com.graymatter.askme.connector.exception.FSCException;
import com.graymatter.askme.connector.kettel.general.FSCGeneralStep;
import com.graymatter.askme.connector.util.FSCConstants;

public class FSCFetchStep extends FSCGeneralStep {
	
	public FSCFetchStep() {
		super();
	}


	public StepMetaInterface getStep() throws Exception {
		
		GetFileNamesMeta gfnm;
		try{
			gfnm = new GetFileNamesMeta();
			gfnm.allocate(1);
			gfnm.setFileName(new String[]{FSCEnvironment.getEnv().get(XMLEnvelopeConstants.FILTER_ENV).get(FSCConstants.FilterConstants.FOLDER_ROOT)});
			gfnm.setFileMask(new String[]{FSCEnvironment.getEnv().get(XMLEnvelopeConstants.FILTER_ENV).get(FSCConstants.FilterConstants.FILE_RULES)});
			gfnm.setIncludeSubFolders(new String[]{"S"});
			gfnm.setFilterFileType(1);
		} catch (Exception exc){
			exc.printStackTrace();
			throw new FSCException(FSCErrorCode.FSFetch.FETCH_GENERIC);
		}
		
		return gfnm;
	}

}
