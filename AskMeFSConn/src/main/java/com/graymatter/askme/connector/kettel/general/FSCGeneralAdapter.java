package com.graymatter.askme.connector.kettel.general;

import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.RowAdapter;

import com.graymatter.askme.common.utility.ASKConstants;
import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;
import com.graymatter.askme.connector.util.FSCConstants;

public abstract class FSCGeneralAdapter extends RowAdapter {
	protected boolean firstRow = true;
	protected LogChannelInterface logger;
	private Integer referenceIndex = null;
	private Integer fileExtensionIndex = null;
	private Integer filenameIndex = null;
	private Integer fileContentIndex = null;
	private Integer titleIndex = null;

	public FSCGeneralAdapter(LogChannelInterface logger) {
		super();
		this.logger = logger;
	}

	protected Integer getReferenceIndex(RowMetaInterface rowMeta){
		if(referenceIndex==null){
			for(int i=0; i<rowMeta.getValueMetaList().size(); i++){
				String tagName = rowMeta.getValueMetaList().get(i).getName();
				if(FSCConstants.FileFetchConstants.FILENAME.equals(tagName)){
					referenceIndex = i;
					break;
				}
			}
		}
		
		return referenceIndex;
	}
	
	protected Integer getExtensionIndex(RowMetaInterface rowMeta){
		if(fileExtensionIndex==null){
			for(int i=0; i<rowMeta.getValueMetaList().size(); i++){
				String tagName = rowMeta.getValueMetaList().get(i).getName();
				if(FSCConstants.FileFetchConstants.FILE_EXT.equals(tagName)){
					fileExtensionIndex = i;
					break;
				}
			}
		}
		
		return fileExtensionIndex;
	}
	
	protected Integer getFilenameIndex(RowMetaInterface rowMeta){
		if(filenameIndex==null){
			for(int i=0; i<rowMeta.getValueMetaList().size(); i++){
				String tagName = rowMeta.getValueMetaList().get(i).getName();
				if(FSCConstants.FileFetchConstants.FILENAME.equals(tagName)){
					filenameIndex = i;
					break;
				}
			}
		}
		
		return filenameIndex;
	}
	
	protected Integer getFileContentIndex(RowMetaInterface rowMeta){
		if(fileContentIndex==null){
			for(int i=0; i<rowMeta.getValueMetaList().size(); i++){
				String tagName = rowMeta.getValueMetaList().get(i).getName();
				if(ASKOutputConstants.TAG_CONTENT.equals(tagName)){
					fileContentIndex = i;
					break;
				}
			}
		}
		
		return fileContentIndex;
	}
	
	
	protected Integer getTitleIndex(RowMetaInterface rowMeta){
		if(titleIndex==null){
			for(int i=0; i<rowMeta.getValueMetaList().size(); i++){
				String tagName = rowMeta.getValueMetaList().get(i).getName();
				if(ASKConstants.ASKOutputConstants.TAG_TITLE.equals(tagName)){
					titleIndex = i;
					break;
				}
			}
		}
		
		return titleIndex;
	}

}
