package com.graymatter.askme.connector.util.fingerPrint;

import java.io.FileInputStream;
import java.io.Serializable;

import org.apache.commons.codec.digest.DigestUtils;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorError.AskMeEncodingError;
import com.graymatter.askme.connector.exception.FSCErrorCode;
import com.graymatter.askme.connector.exception.FSCException;

public class FSCFingerPrintGenerator implements Serializable{

	private static final long serialVersionUID = 1L;
	

	public FSCFingerPrintGenerator() {
		super();
	}
	
	
	public static String generateFingerPrint(FileInputStream file) throws Exception {
		
		String fingerPrint = null;
		try{
			fingerPrint = DigestUtils.md5Hex(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new FSCException(AskMeEncodingError.ENCODING_GENERIC);
		} 
		
		return fingerPrint;
	}
	
	public static String generateFingerPrint(String file) throws Exception {
		
		String fingerPrint = null;
		try{
			fingerPrint = DigestUtils.md5Hex(file);
		} catch (Exception e) {
			e.printStackTrace();
			throw new FSCException(AskMeEncodingError.ENCODING_GENERIC);
		} 
		
		return fingerPrint;
	}
}
