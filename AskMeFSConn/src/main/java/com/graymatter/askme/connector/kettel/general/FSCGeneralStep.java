package com.graymatter.askme.connector.kettel.general;

import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants.UtilityConstants;

public abstract class FSCGeneralStep  extends BaseStepMeta{
	
	protected String getConcatColumn(String columnName, Map<String, String> currentField){
		if(columnName.contains(UtilityConstants.FIELD_SEPARATOR)){
			String separator = Const.isEmpty(currentField.get(UtilityConstants.ATTR_SEPARATOR))?UtilityConstants.FIELD_SEPARATOR:currentField.get(UtilityConstants.ATTR_SEPARATOR);
			columnName = "CONCAT(" + columnName.replace(UtilityConstants.FIELD_SEPARATOR, ",'" + separator + "',") + ")";
		}
		return columnName;
	}

	public abstract StepMetaInterface getStep() throws Exception;

}
