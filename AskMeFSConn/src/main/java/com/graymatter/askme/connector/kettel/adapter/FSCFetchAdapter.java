package com.graymatter.askme.connector.kettel.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.bson.Document;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.logging.LogChannelInterface;
import org.pentaho.di.core.row.RowMetaInterface;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.common.utility.ASKConstants.ASKOutputConstants;
import com.graymatter.askme.common.utility.ASKConstants.ASKParameters;
import com.graymatter.askme.connector.common.bean.AskMeDocument;
import com.graymatter.askme.connector.common.exception.AskMeUtilityThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.common.util.fingerPrint.AskMeFingerPrintGenerator;
import com.graymatter.askme.connector.configuration.FSCEnvironment;
import com.graymatter.askme.connector.kettel.general.FSCGeneralAdapter;
import com.graymatter.askme.connector.persistence.FSCMongoConnectionManager;
import com.graymatter.askme.connector.util.FSCConstants;
import com.graymatter.askme.connector.util.FSCConstants.FileFetchConstants;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;

public class FSCFetchAdapter extends FSCGeneralAdapter {
	
	public FSCFetchAdapter(LogChannelInterface logger) {
		super(logger);
	}

	
	public void rowReadEvent(RowMetaInterface rowMeta, Object[] row) throws KettleStepException {}
	
	public void rowWrittenEvent(RowMetaInterface rowMeta,Object[] row) throws KettleStepException {
		
		FSCMongoConnectionManager connectionManager = null;
		try {
			String filePath = (String)row[getFilenameIndex(rowMeta)];
			Pattern pattern = Pattern.compile(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.FILTER_ENV).get(FSCConstants.FilterConstants.FOLDER_RULES));
			Matcher matcher = pattern.matcher(filePath);
			
			if(!matcher.find()){
				
				String reference = (String)row[getReferenceIndex(rowMeta)];
				String host = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
				Integer port = Integer.parseInt(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
				String localDbName = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
				
				FileInputStream inputstream = new FileInputStream(new File(filePath));
		        String fingerPrint = AskMeFingerPrintGenerator.generateFingerPrint(inputstream);
		        
		        BodyContentHandler handler = new BodyContentHandler(10*1024*1024);
		        Metadata metadata = new Metadata();
		        metadata.add(Metadata.CONTENT_ENCODING, "UTF-8");
		        ParseContext context = new ParseContext();
		        Parser parser = new AutoDetectParser();
		        parser.parse(new FileInputStream(new File(filePath)), handler, metadata, context);
		        String content = handler.toString();
				
				connectionManager = new FSCMongoConnectionManager(localDbName, host, port);
				String collectionName = FileFetchConstants.PREFIX +
									    FSCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
									    FileFetchConstants.SUFFIX;
				BasicDBObject query = new BasicDBObject(ASKOutputConstants.TAG_FINGERPRINT, fingerPrint);
				FindIterable<Document> cursor = (FindIterable<Document>) connectionManager.find(collectionName, query);
				Iterator<Document> iter = cursor.iterator();
				
				// Trovo il fingerPrint...il documento non è cambiato
				if(iter.hasNext()) {
					Document currentDocument = iter.next();
					currentDocument.put(ASKOutputConstants.TAG_REFERENCE, reference);
					currentDocument.put(ASKOutputConstants.TAG_TITLE, metadata.get(ASKOutputConstants.TAG_TITLE));
					currentDocument.put(ASKOutputConstants.TAG_FLAG, ASKParameters.FLAG_EQUAL);
					connectionManager.update(collectionName, query, currentDocument);
				}
				// Non trovo il fingerPrint...cerco il file con la reference
				else{
					query = new BasicDBObject(ASKOutputConstants.TAG_REFERENCE, reference);
					cursor = (FindIterable<Document>) connectionManager.find(collectionName, query);
					iter = cursor.iterator();
					
					// Trovo la reference....è cambiato il content
					if(iter.hasNext()) {
						Document currentDocument = iter.next();
						currentDocument.put(ASKOutputConstants.TAG_REFERENCE, reference);
						currentDocument.put(ASKOutputConstants.TAG_TITLE, metadata.get(ASKOutputConstants.TAG_TITLE));
						currentDocument.put(ASKOutputConstants.TAG_CONTENT, content);
						currentDocument.put(ASKOutputConstants.TAG_FINGERPRINT, fingerPrint);
						currentDocument.put(ASKOutputConstants.TAG_FLAG, ASKParameters.FLAG_UPDATE);
						connectionManager.update(collectionName, query, currentDocument);
					}
					// Non trovo la reference...il documento è nuovo
					else{
						AskMeDocument currentAskMeDocument = new AskMeDocument();
					    currentAskMeDocument.setReference(reference);
					    currentAskMeDocument.setTitle(metadata.get(ASKOutputConstants.TAG_TITLE));
					    currentAskMeDocument.setContent(content);
					    currentAskMeDocument.setFingerPrint(fingerPrint);
					    currentAskMeDocument.setFlag(ASKParameters.FLAG_INSERT);
					    
					    Document bsonDocument = AskMeDocument.createDBObject(currentAskMeDocument);
					    connectionManager.insert(collectionName, bsonDocument);
					}
				}
			}
		} 
		catch (ASKPersistenceThrow e) {
			throw new KettleStepException(e);
		}
		catch (AskMeUtilityThrow e) {
			throw new KettleStepException(e);
		}
		catch (Exception e) {
			throw new KettleStepException(e);
		} 
		finally{
			try {
				if(connectionManager != null) connectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
	}
}
