package com.graymatter.askme.connector.exception;

public class FSCErrorCode {
	
	public class DBError{
		public static final String DB_GENERIC = 			"FSC-DB-000";
	}

	public class FSFetch{
		public static final String FETCH_GENERIC = 			"FSC-FETCH-000";
		public static final String FETCH_NOFIELDS = 		"FSC-FETCH-001"; //Non fields defined in configuration
	}
	
	public class FSMongoDB{
		public static final String MONGODB_GENERIC = 		"FSC-MONGODB-000";
	}

	public class Output{
		public static final String OUTPUT_GENERIC = 		"FSC-OUTPUT-000";
		
	}
	
	public class Mapping{
		public static final String MAPPING_GENERIC = 		"DBC-MAPPING-000";
		
	}

	public class Persistence{
		public static final String FSC_CONN_NULL 	= "FSC-000";//Connection NULL
		public static final String FSC_SELECT 		= "FSC-001";//Eccezione in fase di select
		public static final String FSC_INSERT 		= "FSC-002";//Eccezione in fase di insert
		public static final String FSC_UPDATE 		= "FSC-003";//Eccezione in fase di update
		public static final String FSC_DELETE 		= "FSC-004";//Eccezione in fase di delete
		public static final String FSC_CREATE 		= "FSC-005";//Exception during CREATE TABLE
		public static final String FSC_DESCR 		= "FSC-006";//Exception during DESCR TABLE
		public static final String FSC_TRUNCATE 	= "FSC-007";//Exception during TRUNCATE TABLE
		public static final String FSC_DROP 		= "FSC-008";//Exception during DROP TABLE
	}

}
