package com.graymatter.askme.connector.kettel;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.xml.XMLHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.FSCEnvironment;

public class FSCLauncher {
	private String path;
	
	public FSCLauncher(String path) {
		super();
		this.path = path;
	}

	public void launch() throws Exception{
		String xml = FileUtils.readFileToString(new File(path + File.separator + "conf.xml"));

		Document doc = XMLHandler.loadXMLString(xml);
		Node conf = XMLHandler.getSubNode(doc, XMLEnvelopeConstants.STARTER);
		FSCEnvironment.initConf(path, conf);
		List<Node> jobList = XMLHandler.getNodes(conf, XMLEnvelopeConstants.JOB_ENV);
		for(int i=0; i<jobList.size(); i++){
			try{
				Node job = jobList.get(i);
				FSCEnvironment.initJob(path,job);
				
				String disabled = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_DISABLED);
				Boolean isDisabled = (Const.isEmpty(disabled) || "false".equalsIgnoreCase(disabled))?new Boolean(false):new Boolean(true);

				if(!isDisabled){
					FSCEnvironment.initLocalMongoCollection();
					FSCProcess process = new FSCProcess();
					process.execute();
					FSCEnvironment.writeRemoteMongoCollection();
				}
			}catch(ASKPersistenceThrow askPersistenceExc){
				askPersistenceExc.printStackTrace();
			}catch (Exception exc) {
				exc.printStackTrace();
			}finally{
				FSCEnvironment.release();
			}
		}
	}
	
	public static void main(String[] args) {
		try{
			FSCLauncher launcher = new FSCLauncher("F:\\Documenti\\Ask.Me\\FSConnector");
			launcher.launch();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

}
