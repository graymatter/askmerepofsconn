package com.graymatter.askme.connector.kettel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.Result;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.logging.LogLevel;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransHopMeta;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.FSCEnvironment;
import com.graymatter.askme.connector.exception.FSCException;
import com.graymatter.askme.connector.kettel.adapter.FSCFetchAdapter;
import com.graymatter.askme.connector.kettel.general.FSCGeneralStep;
import com.graymatter.askme.connector.kettel.step.FSCFetchStep;
import com.graymatter.askme.connector.kettel.step.FSCFilterStep;

public class FSCProcess {
	DatabaseMeta dataBaseMeta;
	Map<String, StepMeta> mapSteps = new LinkedHashMap<String, StepMeta>();

	private StepMeta getStepMeta(StepMetaInterface step, String name, String description) throws Exception{
		StepMeta stepMeta = new StepMeta(name, step);
		stepMeta.setDescription(description);
		return stepMeta;
	}
	
	private StepMeta addStep (TransMeta currentTrans, FSCGeneralStep currentStep, String stepName, String stepDescription) throws Exception{
		StepMetaInterface step = currentStep.getStep();
		StepMeta stepMeta = getStepMeta(step, stepName, stepDescription);
		currentTrans.addStep(stepMeta);
		return stepMeta;
	}

	private TransHopMeta getHop(StepMeta from, StepMeta to) throws Exception{
		TransHopMeta hop = new TransHopMeta(from, to);
		return hop;
	}
	
	private void createHop(TransMeta currentTrans) throws Exception{
		Set<String> keySet = mapSteps.keySet();
		ArrayList<String> keyList = new ArrayList<String>(keySet);
		for(int i=0; i<keyList.size()-1; i++){
			TransHopMeta hop = getHop(mapSteps.get(keyList.get(i)), mapSteps.get(keyList.get(i+1)));
			currentTrans.addTransHop(hop);
		}
	}
	
	private TransMeta getTransDefinition() throws Exception{
		TransMeta transMeta = new TransMeta();
		try {
			transMeta.setName(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME));
			
			mapSteps.put("FETCH FILE", addStep(transMeta, new FSCFetchStep(), "FETCH FILE", "Reading Files from FileSystem"));
			mapSteps.put("FILTER", addStep(transMeta,new FSCFilterStep(),"FILTER","Remove row if reference is null"));			
			createHop(transMeta);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new FSCException();
		}
		return transMeta;
	}

	public void execute() throws Exception{
		try{
			KettleEnvironment.init();
			TransMeta transMeta = getTransDefinition();
			Trans trans = new Trans(transMeta);
			trans.setLogLevel(LogLevel.DEBUG);
			trans.prepareExecution(null);
			
			
			StepInterface step = trans.getStepInterface("FETCH FILE", 0);
			step.addRowListener(new FSCFetchAdapter(trans.getLogChannel()));
			
			trans.startThreads();
			trans.waitUntilFinished();
			
			Result result = trans.getResult();
			
			// report on the outcome of the transformation
			String outcome = "executed "+ (result.getNrErrors() == 0 ? "successfully": "with " + result.getNrErrors()+ " errors")+"\n";
			trans.getLogChannel().logBasic(outcome);
			
		}catch(Exception e){
			throw e;
		}
	}
}
