package com.graymatter.askme.connector.kettel.step;

import org.bson.Document;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.pentaho.di.trans.steps.dummytrans.DummyTransMeta;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.configuration.FSCEnvironment;
import com.graymatter.askme.connector.exception.FSCErrorCode;
import com.graymatter.askme.connector.exception.FSCException;
import com.graymatter.askme.connector.kettel.general.FSCGeneralStep;
import com.graymatter.askme.connector.persistence.FSCMongoConnectionManager;
import com.graymatter.askme.connector.util.FSCConstants;
import com.graymatter.askme.connector.util.FSCConstants.FileFetchConstants;
import com.mongodb.client.MongoCollection;

public class FSCMongoOutputStep extends FSCGeneralStep {
	
	public FSCMongoOutputStep() {
		super();
	}

	public StepMetaInterface getStep() throws Exception {
		
		FSCMongoConnectionManager localConnectionManager = null;
		FSCMongoConnectionManager remoteConnectionManager = null;
		
		try{
			String localHost = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String remoteHost = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer remotePort = Integer.parseInt(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String remoteDbName = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String collectionName = FileFetchConstants.PREFIX +
								    FSCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
								    FileFetchConstants.SUFFIX;
			
			localConnectionManager = new FSCMongoConnectionManager(localDbName, localHost, localPort);
			MongoCollection<Document> localContentTableCollection = localConnectionManager.getCollection(collectionName);
			
			if(localContentTableCollection.count() > 0){
				remoteConnectionManager = new FSCMongoConnectionManager(remoteDbName, remoteHost, remotePort);
				remoteConnectionManager.createCollection(localContentTableCollection, collectionName);
			}
		} 
		catch (ASKPersistenceThrow e) {
			throw new KettleStepException(e);
		}
		catch (Exception exc){
			exc.printStackTrace();
			throw new FSCException(FSCErrorCode.FSMongoDB.MONGODB_GENERIC);
		}
		finally{
			try {
				if(localConnectionManager != null) localConnectionManager.closeClient();
				if(remoteConnectionManager != null) remoteConnectionManager.closeClient();
			} catch (ASKPersistenceThrow e) {
				e.printStackTrace();
			}
		}
		
		return new DummyTransMeta();
	}

}
