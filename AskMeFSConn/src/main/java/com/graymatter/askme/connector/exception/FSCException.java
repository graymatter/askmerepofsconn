package com.graymatter.askme.connector.exception;

@SuppressWarnings("serial")
public class FSCException extends Exception {

	public FSCException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FSCException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FSCException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FSCException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
