package com.graymatter.askme.connector.util;


public class FSCConstants {

	public static class PersistenceConstants{
		public static final String CONN_STRING = 	"jdbc:h2:file:[PATH]/data/[NAME]";
		public static final String CONN_USER = 		"GRAY";
		public static final String CONN_PWD = 		"MATTER";
	}
	
	public static class FileFetchConstants{
		public static final String PREFIX			=	"FSC_";
		public static final String SUFFIX			=	"_TABLE";
		public static final String PATH 	    	=   "path";
		public static final String FILENAME 		=   "filename";
		public static final String FILE_EXT 		=	"extension";
	}
	
	public static class FileTypeConstants{
		public static final String PDF  =   "pdf";
		public static final String TXT  =   "txt";
		public static final String DOC  =   "doc";
		public static final String DOCX =   "docx";
	}
	
	public static class FilterConstants{
		public static final String FOLDER_ROOT 	    = "folderRootToScan";
		public static final String FOLDER_RULES 	= "scannerFolderRules";
		public static final String FILE_RULES 		= "scannerFileRules";
	}

}
