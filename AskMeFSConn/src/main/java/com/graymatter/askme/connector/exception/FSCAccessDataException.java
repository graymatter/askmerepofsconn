package com.graymatter.askme.connector.exception;

@SuppressWarnings("serial")
public class FSCAccessDataException extends Exception {

	public FSCAccessDataException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FSCAccessDataException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FSCAccessDataException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FSCAccessDataException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
