package com.graymatter.askme.connector.configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.xml.XMLHandler;
import org.w3c.dom.Node;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.JobConstants;
import com.graymatter.askme.connector.common.util.constants.AskMeConnectorCommonConstants.XMLEnvelopeConstants;
import com.graymatter.askme.connector.common.util.mongoUtil.MongoUtility;
import com.graymatter.askme.connector.persistence.FSCMongoConnectionManager;
import com.graymatter.askme.connector.util.FSCConstants;
import com.graymatter.askme.connector.util.FSCConstants.FileFetchConstants;

public class FSCEnvironment {
	private static Map<String, Map<String, String>> env;
	private static List<Map<String, String>> envFields;
	private static String PATH;
	
	public static Map<String, Map<String, String>> getEnv(){
		return env;
	}
	
	public static List<Map<String, String>> getEnvFields(){
		return envFields;
	}
	
	private static void makeJobEnv(Node job) throws Exception{
		Map<String, String> jobEnv = new HashMap<String, String>();
 		jobEnv.put(JobConstants.PATH, PATH);

		for(int j=0; j<JobConstants.getAttribute().length; j++){
			String currentAttr = JobConstants.getAttribute()[j];
			String attrValue = XMLHandler.getTagAttribute(job, currentAttr);
			if(!Const.isEmpty(attrValue)) jobEnv.put(currentAttr, attrValue);
		}
		
		env.put(XMLEnvelopeConstants.JOB_ENV, jobEnv);
	}
	
	private static void makeLocalDBEnv(Node job) throws Exception{
		Map<String, String> dbEnv = new HashMap<String, String>();
		dbEnv.put(XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV, XMLEnvelopeConstants.HOSTNAME));
		dbEnv.put(XMLEnvelopeConstants.PORT,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV, XMLEnvelopeConstants.PORT));
		dbEnv.put(XMLEnvelopeConstants.DBTYPE,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.DBTYPE));
		dbEnv.put(XMLEnvelopeConstants.ACCESS,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.ACCESS));
		dbEnv.put(XMLEnvelopeConstants.DBNAME,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.DBNAME));
		dbEnv.put(XMLEnvelopeConstants.USER,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.USER));
		dbEnv.put(XMLEnvelopeConstants.PWD,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.LOCAL_DATABASE_ENV,  XMLEnvelopeConstants.PWD));
		env.put(XMLEnvelopeConstants.LOCAL_DATABASE_ENV, dbEnv);
	}
	
	private static void makeRemoteDBEnv(Node job) throws Exception{
		Map<String, String> dbEnv = new HashMap<String, String>();
		dbEnv.put(XMLEnvelopeConstants.HOSTNAME,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV, XMLEnvelopeConstants.HOSTNAME));
		dbEnv.put(XMLEnvelopeConstants.PORT,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV, XMLEnvelopeConstants.PORT));
		dbEnv.put(XMLEnvelopeConstants.DBTYPE,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.DBTYPE));
		dbEnv.put(XMLEnvelopeConstants.ACCESS,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.ACCESS));
		dbEnv.put(XMLEnvelopeConstants.DBNAME,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.DBNAME));
		dbEnv.put(XMLEnvelopeConstants.USER,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.USER));
		dbEnv.put(XMLEnvelopeConstants.PWD,  XMLHandler.getTagValue(job, XMLEnvelopeConstants.REMOTE_DATABASE_ENV,  XMLEnvelopeConstants.PWD));
		env.put(XMLEnvelopeConstants.REMOTE_DATABASE_ENV, dbEnv);
	}
	
//	private static void makeSqlEnv(Node job) throws Exception{
//		Map<String, String> sqlEnv = new HashMap<String, String>();
//		sqlEnv.put(FSCConstants.SQLConstants.SELECT, XMLHandler.getTagValue(job, FSCConstants.XMLEnvelopeConstants.SQL_ENV, FSCConstants.SQLConstants.SELECT));
//		sqlEnv.put(FSCConstants.SQLConstants.PRIMARY_KEY, XMLHandler.getTagValue(job, FSCConstants.XMLEnvelopeConstants.SQL_ENV, FSCConstants.SQLConstants.PRIMARY_KEY));
//		
//		Node fields = XMLHandler.getSubNode(job, XMLEnvelopeConstants.SQL_ENV, FSCConstants.SQLConstants.FIELDS);
//		List<Node> nl = XMLHandler.getNodes(fields, FSCConstants.SQLConstants.FIELD);
//		for(int i=0; i<nl.size(); i++){
//			if(envFields==null) envFields = new ArrayList<Map<String,String>>();
//			Node currentNode = nl.get(i);
//			String value =  XMLHandler.getNodeValue(currentNode);
//			
//			Map<String,String> field = new LinkedHashMap<String, String>();
//			field.put(FSCConstants.SQLConstants.FIELD, value);
//			for(int j=0; j<FSCConstants.SQLConstants.getAttribute().length; j++){
//				String currentAttr = FSCConstants.SQLConstants.getAttribute()[j];
//				String attrValue = XMLHandler.getTagAttribute(currentNode, currentAttr);
//				if(!Const.isEmpty(attrValue)) field.put(currentAttr, attrValue);
//			}
//			envFields.add(field);
//		}
//		
//		env.put(FSCConstants.XMLEnvelopeConstants.SQL_ENV, sqlEnv);
//	}
	
	private static void makeFilterEnv(Node job) throws Exception{
		Map<String, String> filterEnv = new HashMap<String, String>();
		filterEnv.put(FSCConstants.FilterConstants.FOLDER_ROOT, XMLHandler.getTagValue(job, XMLEnvelopeConstants.FILTER_ENV, FSCConstants.FilterConstants.FOLDER_ROOT));
		filterEnv.put(FSCConstants.FilterConstants.FOLDER_RULES, XMLHandler.getTagValue(job, XMLEnvelopeConstants.FILTER_ENV, FSCConstants.FilterConstants.FOLDER_RULES));
		filterEnv.put(FSCConstants.FilterConstants.FILE_RULES, XMLHandler.getTagValue(job, XMLEnvelopeConstants.FILTER_ENV, FSCConstants.FilterConstants.FILE_RULES));
		env.put(XMLEnvelopeConstants.FILTER_ENV, filterEnv);
	}
	
	public static void initJob(String path, Node job) throws Exception{
		if(env==null) env = new HashMap<String, Map<String,String>>();
		PATH = path;
		makeJobEnv(job);
		makeRemoteDBEnv(job);
//		makeSqlEnv(job);
		makeFilterEnv(job);
	}
	
	public static void initConf(String path, Node conf) throws Exception{
		if(env==null) env = new HashMap<String, Map<String,String>>();
		PATH = path;
		makeLocalDBEnv(conf);
	}
	
	public static void release(){
		env = null;
		envFields = null;
	}

public static void initLocalMongoCollection() throws ASKPersistenceThrow, Exception{
		
		FSCMongoConnectionManager localConnectionManager;
		FSCMongoConnectionManager remoteConnectionManager;
		
		try{
			String localHost = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String remoteHost = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer remotePort = Integer.parseInt(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String remoteDbName = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String collectionName = FileFetchConstants.PREFIX +
								    FSCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
								    FileFetchConstants.SUFFIX;
			
			localConnectionManager = new FSCMongoConnectionManager(localDbName, localHost, localPort);
			remoteConnectionManager = new FSCMongoConnectionManager(remoteDbName, remoteHost, remotePort);
			
			MongoUtility mongoUtility = new MongoUtility(localConnectionManager, remoteConnectionManager, collectionName);
			mongoUtility.fromRemoteToLocal();
		}
		catch(ASKPersistenceThrow e){
			throw e;
		}
		catch(Exception e){
			throw e;
		}
	}
	
	
	public static void writeRemoteMongoCollection() throws ASKPersistenceThrow, Exception{
		
		FSCMongoConnectionManager localConnectionManager;
		FSCMongoConnectionManager remoteConnectionManager;
		
		try{
			String remoteHost = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer remotePort = Integer.parseInt(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String remoteDbName = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.REMOTE_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String localHost = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.HOSTNAME);
			Integer localPort = Integer.parseInt(FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.PORT));
			String localDbName = FSCEnvironment.getEnv().get(XMLEnvelopeConstants.LOCAL_DATABASE_ENV).get(XMLEnvelopeConstants.DBNAME);
			
			String collectionName = FileFetchConstants.PREFIX +
								    FSCEnvironment.getEnv().get(XMLEnvelopeConstants.JOB_ENV).get(JobConstants.ATTR_NAME).replace(" ", "_") +
								    FileFetchConstants.SUFFIX;
			
			localConnectionManager = new FSCMongoConnectionManager(localDbName, localHost, localPort);
			remoteConnectionManager = new FSCMongoConnectionManager(remoteDbName, remoteHost, remotePort);
			
			MongoUtility mongoUtility = new MongoUtility(localConnectionManager, remoteConnectionManager, collectionName);
			mongoUtility.fromLocalToRemote();
		}
		catch(ASKPersistenceThrow e){
			throw e;
		}
		catch(Exception e){
			throw e;
		}
	}
}
