package com.graymatter.askme.connector.persistence;

import java.io.Serializable;
import java.util.Collection;

import org.bson.Document;

import com.graymatter.askme.common.exception.ASKPersistenceThrow;
import com.graymatter.askme.common.mongo.ASKMongoGeneral;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;


public class FSCMongoConnectionManager extends ASKMongoGeneral implements Serializable {

	public FSCMongoConnectionManager(String dbName, String hostName, int hostPort) throws Exception {
		super(dbName, hostName, hostPort);
	}

	private static final long serialVersionUID = 1L;
		
}
